<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="Prometheus workshop">
		<meta name="author" content="Eric D. Schabell">

		<title>Lab 2 - Prometheus binary</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/chrono.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-PW25SPMWGG"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-PW25SPMWGG');
	</script>

	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h3 class="r-fit-text">Lab 2 - Installing Prometheus (binary package)</h3>
				</section>
				<section>
					<div style="height: 250px;">
						<h2>Lab Goal</h2>
						<h4>This lab guides you through installing Prometheus from a pre-compiled binary package on your
							local machine, configuring, and running it to start gathering metrics.</h4>
					</div>
				</section>

				<section>
					<div style="height: 120px;">
						<h3 class="r-fit-text">Prometheus - Installation notes</h3>
					</div>
					<div style="height: 350px; text-align: left; font-size: xx-large;">
						This lab guides you through installing Prometheus using one of the pre-compiled binaries. You
						will see this done here for Mac OSX, but links will be provided for both Linux and Windows
						systems. It is expected that if you are using either of these operating systems, that you have
						enough knowledge to apply the steps from this guide using the specific system tooling provided
						by your local machine.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Installation - Make a project directory</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						The first step will be to open a console or terminal window and start with the command line
						to create yourself an empty workshop directory, something like this:
					</div>
					<div style="height: 250px;">
						<pre>
							<code data-trim data-noescape>
								$ mkdir workshop-prometheus

								$ cd workshop-prometheus
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Installation - Download Prometheus</h3>
					</div>
					<div style="height: 350px; text-align: left; font-size: xx-large;">
						Next up you will need to download the Prometheus binary (one that matches your local machine
						operating system) and unzip it in the <code>workshop-directory</code>:<br />
						<br />
						<ul>
							<li>
								Prometheus 3.0.1
								<a href="https://github.com/prometheus/prometheus/releases/download/v3.0.1/prometheus-3.0.1.darwin-amd64.tar.gz" target="_blank">amd64</a>
								(Mac OSX / Darwin)
							</li>
							<li>
								Prometheus 3.0.1
								<a href="https://github.com/prometheus/prometheus/releases/download/v3.0.1/prometheus-3.0.1.linux-amd64.tar.gz" target="_blank"> amd64</a>
								or
								<a href="https://github.com/prometheus/prometheus/releases/download/v3.0.1/prometheus-3.0.1.linux-386.tar.gz" target="_blank">i386</a>
								(Linux)
							</li>
							<li>
								Prometheus 3.0.1
								<a href="https://github.com/prometheus/prometheus/releases/download/v3.0.1/prometheus-3.0.1.windows-amd64.zip" target="_blank">amd64</a>
								or
								<a href="https://github.com/prometheus/prometheus/releases/download/v3.0.1/prometheus-3.0.1.windows-386.zip" target="_blank">i386</a>
								(Windows)
							</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Installation - Unpacking the binary </h3>
					</div>
					<div style="height: 70px; text-align: left; font-size: xx-large;">
						Unpacking the download should look something like this (note the version might
						be different by the time you take this workshop):
					</div>
					<div style="height: 200px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ tar -xzvf prometheus-3.0.1.darwin-amd64.tar.gz

								x prometheus-3.0.1.darwin-amd64/
								x prometheus-3.0.1.darwin-amd64/promtool
								x prometheus-3.0.1.darwin-amd64/LICENSE
								x prometheus-3.0.1.darwin-amd64/prometheus
								x prometheus-3.0.1.darwin-amd64/prometheus.yml
								x prometheus-3.0.1.darwin-amd64/NOTICE
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Installation - Exploring the tools</h3>
					</div>
					<div style="height: 70px; text-align: left; font-size: xx-large;">
						There are four items you just unpacked that are of interest to us:
					</div>
					<div style="height: 350px; text-align: left; font-size: xx-large;">
						<ul>
							<li><code><b>prometheus</b></code> - the binary executable for Prometheus</li>
							<li><code><b>promtool</b></code> - a command line configuration validation tool</li>
							<li><code><b>prometheus.yml</b></code> - simple configuration to run Prometheus</li>
						</ul><br />
						<br />
						We will be making the most use of the Prometheus binary and the basic configuration file
						in the rest of this workshop.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Setup - Copy basic configuration</h3>
					</div>
					<div style="height: 130px; text-align: left; font-size: xx-large;">
						First we will make a copy of the provided configuration file for use in the rest of this
						workshop. We need to move into the Prometheus directory and then make the copy:
					</div>
					<div style="height: 250px;">
						<pre>
							<code data-trim data-noescape>
								$ cd prometheus-3.0.1.darwin-amd64

								$ cp prometheus.yml workshop-prometheus.yml
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Setup - Workshop configuration</h3>
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						Open the copied file <code><b>workshop-prometheus.yml</b></code> and you should see a lot of comments
						spread over the file (something like 25-30 lines). By default it's set up to scrape metrics
						from itself every 15 seconds. Using your favorite editor, clean it up a bit so that it looks
						like this (be sure to save the results):
					</div>
					<div style="height: 50px;">
						<pre>
							<code data-trim data-noescape>
								# workshop config
								global:
								  scrape_interval: 5s

								# Scraping only Prometheus.
								scrape_configs:
								  - job_name: "prometheus"
								    static_configs:
								      - targets: ["localhost:9090"]
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Configuration - Some thoughts on setup</h3>
					</div>
					<div style="height: 300px; text-align: left; font-size: xx-large;">
						Normally you are monitoring other targets over HTTP and scraping their endpoints, but we are
						going to start with Prometheus as it also exposes its own metrics endpoints. Monitoring your
						Prometheus servers' health is only an exercise for this workshop. Also note that scraping metrics
						every 5 seconds is a bit over the top, commonly you would see 10-60 seconds, but we want our
						data to flow in a steady stream for this workshop.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Configuration - The global section</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						As you can imagine, the <code><b>global</b></code> section is used for settings and default values.
						Here we have just set the default <code><b>scrape interval</b></code> to be 5 seconds:
					</div>
					<div style="height: 200px;">
						<pre>
							<code data-trim data-noescape>
								# workshop config
								global:
								  scrape_interval: 5s
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Configuration - The scrape configs section</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						The <code><b>scrape_configs</b></code> section is where you tell Prometheus which targets to scrape to
						collect metrics from. In the beginning we will be listing each job for our targets manually,
						using a <code><b>host:port</b></code> format:
					</div>
					<div style="height: 170px;">
						<pre>
							<code data-trim data-noescape>
								# Scraping only Prometheus.
								scrape_configs:
								  - job_name: "prometheus"
								    static_configs:
								      - targets: ["localhost:9090"]
							</code>
						</pre>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						Note: production configurations would use service discovery integrations to find targets, more
						on that later in this workshop.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Start your metrics engines!</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: x-large;">
						Now it's time to start the Prometheus server. We will point to our config file using a flag, if
						you don't it's going to grab the default <code><b>prometheus.yml</b></code> file. Also note, the in
						memory database is stored by default in <code><b>./data</b></code>. Note: might need to approve the
						file in security settings if it fails to start. (scroll to view log):
					</div>
					<div style="height: 170px; font-size: x-large;">
						<pre>
							<code data-trim data-noescape>
								$ ./prometheus --config.file=workshop-prometheus.yml

								...
								time=2024-12-10T14:21:38.421+01:00 level=INFO source=main.go:642 msg="No time or size retention was set so using the default time retention" duration=15d
								time=2024-12-10T14:21:38.422+01:00 level=INFO source=main.go:689 msg="Starting Prometheus Server" mode=server version="(version=3.0.1, branch=HEAD, revision=1f56e8492c31a558ccea833027db4bd7f8b6d0e9)"
								time=2024-12-10T14:21:38.422+01:00 level=INFO source=main.go:694 msg="operational information" build_context="(go=go1.23.3, platform=darwin/amd64, user=root@6b1b3f1faf28, date=20241128-17:20:48, tags=netgo,builtinassets,stringlabels)" host_details=(darwin) fd_limits="(soft=61440, hard=unlimited)" vm_limits="(soft=unlimited, hard=unlimited)"
								time=2024-12-10T14:21:38.423+01:00 level=INFO source=main.go:770 msg="Leaving GOMAXPROCS=8: CPU quota undefined" component=automaxprocs
								time=2024-12-10T14:21:38.425+01:00 level=INFO source=web.go:650 msg="Start listening for connections" component=web address=0.0.0.0:9090
								time=2024-12-10T14:21:38.426+01:00 level=INFO source=main.go:1239 msg="Starting TSDB ..."
								time=2024-12-10T14:21:38.429+01:00 level=INFO source=tls_config.go:347 msg="Listening on" component=web address=[::]:9090
								time=2024-12-10T14:21:38.429+01:00 level=INFO source=tls_config.go:350 msg="TLS is disabled." component=web http2=false address=[::]:9090
								time=2024-12-10T14:21:38.432+01:00 level=INFO source=head.go:628 msg="Replaying on-disk memory mappable chunks if any" component=tsdb
								time=2024-12-10T14:21:38.432+01:00 level=INFO source=head.go:715 msg="On-disk memory mappable chunks replay completed" component=tsdb duration=6.125µs
								time=2024-12-10T14:21:38.432+01:00 level=INFO source=head.go:723 msg="Replaying WAL, this may take a while" component=tsdb
								time=2024-12-10T14:21:38.433+01:00 level=INFO source=head.go:795 msg="WAL segment loaded" component=tsdb segment=0 maxSegment=0
								time=2024-12-10T14:21:38.434+01:00 level=INFO source=head.go:832 msg="WAL replay completed" component=tsdb checkpoint_replay_duration=68µs wal_replay_duration=1.348666ms wbl_replay_duration=167ns chunk_snapshot_load_duration=0s mmap_chunk_replay_duration=6.125µs total_replay_duration=1.70275ms
								time=2024-12-10T14:21:38.435+01:00 level=INFO source=main.go:1260 msg="filesystem information" fs_type=1a
								time=2024-12-10T14:21:38.435+01:00 level=INFO source=main.go:1263 msg="TSDB started"
								time=2024-12-10T14:21:38.435+01:00 level=INFO source=main.go:1446 msg="Loading configuration file" filename=prometheus.yml
								time=2024-12-10T14:21:38.464+01:00 level=INFO source=main.go:1485 msg="updated GOGC" old=100 new=75
								time=2024-12-10T14:21:38.464+01:00 level=INFO source=main.go:1495 msg="Completed loading of configuration file" db_storage=5.041µs remote_storage=2.167µs web_handler=459ns query_engine=1.875µs scrape=27.376291ms scrape_sd=32.084µs notify=2.167µs notify_sd=917ns rules=2.583µs tracing=125.541µs filename=prometheus.yml totalDuration=28.635708ms
								time=2024-12-10T14:21:38.464+01:00 level=INFO source=main.go:1224 msg="Server is ready to receive web requests."
								time=2024-12-10T14:21:38.464+01:00 level=INFO source=manager.go:168 msg="Starting rule manager..." component="rule manager"
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Adjusting settings live</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						If you change settings in a configuration file from a live running Prometheus server, you can
						apply them by sending a <code>HUP</code> signal on your platform or by reloading via the HTTP
						API. Any changes you need to make that were in the flags used to start the server will require
						a full server restart to apply.<br />
						<br />
						Now let's see if our Prometheus server is up and running on our local machine by loading the
						status page in our browser at <a href="http://localhost:9090" target="_blank">http://localhost:9090</a>,
						noting it needs to run a little bit to collect some data from its own HTTP metrics endpoint.
					</div>
				</section>


				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - The status page</h3>
					</div>
					<div style="height: 350px;">
						<img src="images/lab02-1.png" alt="status"><br />
					</div>
					<div style="height: 50px; text-align: left; font-size: x-large;">
						Now try the dark mode feature by clicking on the half moon icon in the top right corner.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Status page (dark mode)</h3>
					</div>
					<div style="height: 350px;">
						<img src="images/lab02-2.png" alt="status"><br />
					</div>
					<div style="height: 50px; text-align: left; font-size: x-large;">
						Now try the metrics endpoint
						<a href="http://localhost:9090/metrics" target="_blank">(http://localhost:9090/metrics)</a>
						directly in your browser.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Live metrics endpoint</h3>
					</div>
					<div style="height: 450px;">
						<img src="images/lab02-3.png" alt="endpoint"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Checking your targets</h3>
					</div>
					<div style="height: 150px; text-align: left; font-size: xx-large;">
						After you configure a new <code><b>prometheus</b></code> target to scrape and (re)start the
						Prometheus server, validate it's running correctly by going to the status page, using the
						drop down menu at the top labeled <code><b>STATUS</b></code> and selecting
						<code><b>TARGETS</b></code>:
					</div>
					<div style="height: 350px;">
						<img src="images/lab02-4.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Checking your targets</h3>
					</div>
					<div style="height: 130px; text-align: left; font-size: xx-large;">
						This shows you a list of the targets, in our case just one, featuring the scrape configuration
						details. The most important field here is the <code><b>STATE</b></code>, where we want to see a green
						<code><b>UP</b></code>:
					</div>
					<div style="height: 300px;">
						<img src="images/lab02-5.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Bad target state</h3>
					</div>
					<div style="height: 170px; text-align: left; font-size: xx-large;">
						Let's break our configuration and see what that looks like in the targets status page. To do this
						open up your configuration file <code><b>workshop-prometheus.yml</b></code> and edit the
						<code><b>scrape_configs</b></code> section to alter the targets port number as shown:
					</div>
					<div style="height: 170px;">
						<pre>
							<code data-trim data-noescape>
								# Scraping only Prometheus.
								scrape_configs:
								  - job_name: "prometheus"
								    static_configs:
								      - targets: ["localhost:9099"]
							</code>
						</pre>
					</div>
					<div style="height: 100px; text-align: left; font-size: xx-large;">
						Save and see next slide for applying the new configuration.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Apply a new configuration</h3>
					</div>
					<div style="height: 150px; text-align: left; font-size: x-large;">
						We mentioned you can restart the Prometheus, that is, stop it and then restart when a new
						configuration needs to be applied. That's going to mean you lose a time period of collecting
						time series data, so let's send a restart signal instead using the <code><b>kill</b></code>
						command. First we find the Prometheus server process id (PID) using one command, then apply it
						using the command as shown below:
					</div>
					<div style="height: 100px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								# Locate the Prometheus process id (PID).
								#
								$ ps aux  | grep prometheus

								erics  94110   1:31PM   0:05.28 ./prometheus --config.file=workshop-prometheus.yml
								erics  97648   2:43PM   0:00.00 grep prometheus

								# Send a restart signal to the PID we found.
								#
								$ kill -s HUP 94110
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Verify bad target state</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						Back to the target state page and we see that indeed the target we configured is broken:
					</div>
					<div style="height: 300px;">
						<img src="images/lab02-10.png" alt="broken"><br />
					</div>
					<div style="height: 100px; text-align: left; font-size: xx-large;">
						<b>Exercise:</b> go back and revert or fix this target and restart the instance before proceeding!
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring command-line flags</h3>
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						When you started Prometheus we mentioned a flag to point at our new configuration file. There is
						a status page you can use to view all the flags that have been set (by your or default settings).
						Using the drop down menu again at the top labeled <code><b>STATUS</b></code> and selecting
						<code><b>COMMAND-LINE FLAGS</b></code>:
					</div>
					<div style="height: 350px;">
						<img src="images/lab02-6.png" alt="menu"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring command-line flags</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						This shows you a long list of flags with their current values and a search field for locating
						one you might be interested in:
					</div>
					<div style="height: 400px;">
						<img src="images/lab02-6a.png" alt="flags"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Searching for a flag</h3>
					</div>
					<div style="height: 100px; text-align: left; font-size: xx-large;">
						By filling in a search query, you can narrow down the long list to a specific area. Let's explore
						the flag value for <code><b>--config.file</b></code>:
					</div>
					<div style="height: 300px;">
						<img src="images/lab02-7.png" alt="config"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Time series database status</h3>
					</div>
					<div style="height: 130px; text-align: left; font-size: xx-large;">
						Next, there is a status page you can use to view your time series database, or TSDB, status.
						Using the drop down menu again at the top labeled <code><b>STATUS</b></code> and selecting
						<code><b>TSDB-STATUS</b></code>:
					</div>
					<div style="height: 300px;">
						<img src="images/lab02-8.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Time series database status</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						This shows you some details for the time series being collected in an overview status line with
						several tables below with cardinality status:
					</div>
					<div style="height: 350px;">
						<img src="images/lab02-8a.png" alt="flags"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring active configuration</h3>
					</div>
					<div style="height: 130px; text-align: left; font-size: xx-large;">
						Lastly, at least in this lab, you can verify the exact configuration being applied to your
						Prometheus server. Using the drop down menu again at the top labeled <code><b>STATUS</b></code>
						and selecting <code><b>CONFIGURATION</b></code>:
					</div>
					<div style="height: 350px;">
						<img src="images/lab02-9.png" alt="starting"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring active configuration</h3>
					</div>
					<div style="height: 150px; text-align: left; font-size: xx-large;">
						This shows you your exact configuration, often including some defaults that you might not have
						in your personal configuration file, yet are begin used. There is even a handy copy-to-clipboard
						button for you to grab it:
					</div>
					<div style="height: 370px;">
						<img src="images/lab02-9a.png" alt="flags"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Using the expression tooling</h3>
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						You can navigate back to the expression browser that let's you query your time series data
						by clicking on the menu entry <code><b>GRAPH</b></code>. This is the default built-in query
						interface for running Prometheus Query Language (PromQL) queries. Be sure you are in the
						<code><b>TABLE</b></code> tab:
					</div>
					<div style="height: 300px;">
						<img src="images/lab02-11.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Total samples ingested</h3>
					</div>
					<div style="height: 280px; text-align: left; font-size: xx-large;">
						The <code><b>TABLE</b></code> view provides the output of a query written using PromQL expression as
						a series. It's less expensive to use than the other option, <code><b>GRAPH</b></code>, because you are
						not plotting out the series answers in a graph. Without worrying about the PromQL used (we'll
						explore that later in this workshop), lets show the total number of samples ingested by our
						Prometheus server since it started:
					</div>
					<div style="height: 200px;">
						<pre>
							<code data-trim data-noescape>
								# Copy this line below comments into the Expression field and
								# click on the EXECUTE button on the right side of the screen.
								#
								prometheus_tsdb_head_samples_appended_total
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Validating an expression</h3>
					</div>
					<div style="height: 150px; text-align: left; font-size: x-large;">
						You will have noticed that there are three buttons to the right of the
						<code><b>EXPRESSION</b></code> entry field. The first one can be used to explore available
						metrics, the second to validate the expression you entered, and the last one to show a tree
						view of complexer queries (this one is not complex). We'll just click on the
						<code><b>FORMAT EXPRESSION</b></code> menu entry and if it's a good expression you get a
						validation pop-up as shown below:
					</div>
					<div style="height: 250px;">
						<img src="images/lab02-12.png" alt="queries"><br />
					</div>
					<div style="height: 100px; text-align: right;">
						<img src="images/lab02-12a.png" alt="pop-up"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Execute the expression query</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						After validating our expression, run it by clicking on the <code><b>EXECUTE</b></code> button:
					</div>
					<div style="height: 300px;">
						<img src="images/lab02-13.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Exploring the visualization</h3>
					</div>
					<div style="height: 70px; text-align: left; font-size: x-large;">
						Let's explore by clicking on the <code><b>GRAPH</b></code> tab. Note the features included to
						visualize queries. I have adjusted this instance to look at 1m of data here and browsing the
						results:
					</div>
					<div style="height: 430px;">
						<img src="images/lab02-13a.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Add another query</h3>
					</div>
					<div style="height: 170px; text-align: left; font-size: xx-large;">
						Go back to the <code><b>TABLE</b></code> tab. Notice at the bottom there is a button
						<code><b>ADD QUERY</b></code>, click on it to add another query panel in which we will execute
						the following query expression. Let's look at the number of samples ingested per second averaged
						over a 1m window of time:
					</div>
					<div style="height: 200px;">
						<pre>
							<code data-trim data-noescape>
								# Copy this line below comments into the Expression field and
								# click on the EXECUTE button on the right side of the screen.
								#
								rate(prometheus_tsdb_head_samples_appended_total[1m])
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Execute the second query</h3>
					</div>
					<div style="height: 50px; text-align: left; font-size: xx-large;">
						You can now see multiple expressions are possible:
					</div>
					<div style="height: 400px;">
						<img src="images/lab02-14.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="height: 50px;">
						<h3 class="r-fit-text">Prometheus - Exploring second visualization</h3>
					</div>
					<div style="height: 550px;">
						<img src="images/lab02-15.png" alt="graphs"><br />
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Intermezzo - Warning about pages</h3>
					</div>
					<div style="height: 350px; text-align: left; font-size: xx-large;">
						If you now select any of the status pages from the <code><b>STATUS</b></code> menu at the top
						and then return to the expression query page using the <code><b>GRAPH</b></code> menu entry, you
						will notice that extra panels you might have added will be gone.<br />
						<br />
						<b>Pro tip:</b> you might want to work using browser tabs.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - One last query</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						Assuming you went to another status page and back, we have a last query we will run here to
						simulate the same query used to fill the system UP metric we viewed for our Prometheus target:
					</div>
					<div style="height: 250px;">
						<pre>
							<code data-trim data-noescape>
								# Copy this line below comments into the Expression field and
								# click on the EXECUTE button on the right side of the screen.
								#
								up{job="prometheus"}
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Prometheus - Execute the second query</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						You can now see the results as a boolean value suggesting it is really UP:
					</div>
					<div style="height: 400px;">
						<img src="images/lab02-16.png" alt="queries"><br />
					</div>
				</section>

				<section>
					<div style="height: 80px;">
						<h3 class="r-fit-text">Prometheus - Exploring this silly visualization</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: x-large;">
						This is a rather silly metric to visualize this way, but this is what it looks like going back
						over 30 mins as my server was running during this lab's development (you can check my working
						hours if we look closely):
					</div>
					<div style="height: 530px;">
						<img src="images/lab02-17.png" alt="graphs"><br />
					</div>
				</section>

				<section>
					<div style="height: 150px;">
						<h2 class="r-fit-text">Lab completed - Results</h2>
					</div>
					<div style="height: 400px; font-size: xx-large">
						<img src="images/lab02-2.png" alt="completed">
						<br />
						Next up, exploring the query language...
					</div>
				</section>

				<section data-background="images/questions.png">
					<span class="menu-title" style="display: none">References</span>
					<div style="height: 200px;">
						<img height="150" width="100%" src="images/references.jpg" alt="references">
					</div>
					<div style="height: 400px; font-size: xx-large; text-align: left">
						<ul>
							<li><a href="https://youtu.be/KJvUyP4c1NQ" target="_blank">Video introduction to workshop (Open Source Edinburgh meetup)</a></li>
							<li><a href="https://o11y-workshops.gitlab.io/" target="_blank">Getting started with cloud native o11y workshops</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-prometheus" target="_blank">This workshop project repository</a></li>
							<li><a href="https://gitlab.com/o11y-workshops" target="_blank">O11y workshop collection</a></li>
							<li><a href="https://prometheus.io" target="_blank">Prometheus project</a></li>
							<li><a href="https://github.com/prometheus/promlens" target="_blank">PromLens project</a></li>
							<li><a href="https://github.com/perses/perses" target="_blank">Perses project</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-prometheus/-/issues/new" target="_blank">Report an issue with this workshop</a></li>
						</ul>
					</div>
				</section>

				<section>
					<span class="menu-title" style="display: none">Questions or feedback?</span>
					<div style="height: 150px">
						<h2 class="r-fit-text">Contact - are there any questions?</h2>
					</div>
					<div style="height: 200px; font-size: x-large; text-align: left">
						Eric D. Schabell<br/>
						Director Evangelism<br/>
						Contact: <a href="https://twitter.com/ericschabell" target="_blank">@ericschabell</a>
						{<a href="https://fosstodon.org/@ericschabell" target="_blank">@fosstodon.org</a>)
						or <a href="https://www.schabell.org" target="_blank">https://www.schabell.org</a>
					</div>
				</section>

				<section>
					<div style="height: 250px;">
						<h2 class="r-fit-text">Up next in workshop... </h2>
					</div>
					<div style="height: 200px; font-size: xxx-large;">
						<a href="lab03.html" target="_blank">Lab 3 - Introduction to the Query Language</a>
					</div>
				</section>
			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMenu, RevealMarkdown, RevealHighlight, RevealNotes ]
			});
		</script>
	</body>
</html>