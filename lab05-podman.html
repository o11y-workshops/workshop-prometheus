<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="Prometheus workshop">
		<meta name="author" content="Eric D. Schabell">

		<title>Lab 5 - PromLens container</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/chrono.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-PW25SPMWGG"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-PW25SPMWGG');
	</script>

	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Podman - Container installation notes</h3>
					</div>
					<div style="height: 350px; text-align: left; font-size: xx-large;">
						This lab guides you through installing PromLens using Podman, an open source container
						tooling set that works on Mac OSX, Linux, and Windows systems. This workshop assumes you have
						a working Podman installation from earlier labs in this workshop.<br />
						<br />
						Note: if you need to install Podman, see the earlier lab for
						<a href="lab02-podman.html" target="_blank">installing Prometheus in a container</a> and return
						here when done.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">PromLens - Pulling prebuilt container image</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						Let's make sure you can access the prebuilt PromLens image by pulling it to our local registry:
					</div>
					<div style="height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman image pull prom/promlens:v0.3.0

								Trying to pull docker.io/prom/promlens:v0.3.0...
								Getting image source signatures
								Copying blob sha256:df56f8c555e8ac1218ae0f5ac530a0c6cd9213b8599a4867e5f0eab93c6ff3da
								Copying blob sha256:15a8001affa70e49b159fb099f3791913c90811f5c6ebad1287328c46aa889dc
								Copying blob sha256:0ae1023ba0140b1fd8ba7ad2cab0d0e0a2ee62cec077cb3c3bf131a3d3b5b79a
								Writing manifest to image destination
								Storing signatures
								f600c282373c05344d94d8122df75710f4686a236a938b3e7cd6edbf91e32347
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">PromLens - Verifying pulled container image</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						Looking up our local images to verify everything went well and we should see something like this:
					</div>
					<div style="height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman images

								REPOSITORY                          TAG          IMAGE ID      CREATED         SIZE
								docker.io/prom/promlens             v0.3.0       f600c282373c  8 months ago    40.2 MB
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">PromLens - Running the container image</h3>
					</div>
					<div style="height: 170px; text-align: left; font-size: xx-large;">
						To run PromLens in a container and not conflict with the default port of 8080, we need to map a
						local port to the container exposed 8080. In this lab we are using 9090 for Prometheus and 8080
						for our services demo, so let's use 8081 for PromLens as follows:
					</div>
					<div style="height: 130px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman run --rm --name promlens -p 8081:8080 prom/promlens:v0.3.0

								ts=2024-10-14T12:33:31.690Z caller=main.go:202 level=info msg="No link sharing backends..."
								ts=2024-10-14T12:33:31.691Z caller=main.go:216 level=info msg="No Grafana backend enabled..."
								ts=2024-10-14T12:33:31.691Z caller=tls_config.go:232 level=info msg="Listening on" addrs=[::]:8080
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">PromLens - Configuring the defaults</h3>
					</div>
					<div style="height: 100px; text-align: left; font-size: x-large;">
						When you open the link <code><b>http://localhost:8081</b></code> you'll see it is not setup to
						query our Prometheus metrics data instance, so we need to adjust the link to point to
						<code><b>http://localhost:9090</b></code> as shown:
					</div>
					<div style="height: 350px;">
						<img src="images/lab05-3.png" alt="default promlens">
						<img src="images/lab05-4.png" alt="prom instance">
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">PromLens - Defaulting to our Prometheus</h3>
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						That was just to show you that it's possible to direct your PromLens instance at any data collection,
						but for now we want this to be defaulting to our Prometheus metrics collection. To achieve this,
						stop the running container (CTRL+C) and start a PromLens container instance as follows:
					</div>
					<div style="height: 50px; font-size: x-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman run --rm --name promlens -p 8081:8080 prom/promlens:v0.3.0 --web.default-prometheus-url "http://localhost:9090"

								751c6909fecee0e6cb67302f37d52aeeb3e421931fe82fa55bd93c895f0c4dcd
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">PromLens - This completes the container installation</h3>
					</div>
					<div style="height: 130px; text-align: left; font-size: xx-large;">
						Next up, we need to verify the installation is working and get started on learning advanced PromQL:
					</div>
					<div style="height: 200px; text-align: center;">
						<a href="lab05b.html" target="_blank">Click here to continue to advanced PromQL</a>
					</div>
				</section>
			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMenu, RevealMarkdown, RevealHighlight, RevealNotes ]
			});
		</script>
	</body>
</html>